<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['posts']=Post::get(); // SELECT * FROM posts;

        $posts=Post::latest()->paginate(5);
        return view('posts.list',compact('posts'))->with('i',(request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title' =>'required',
            'description' =>'required',
        ]);

        if($request->status)
            $status=1;
        else
            $status=0;

        $saved=Post::create([
            'title'=>$request->title,
            'description'=>$request->description,
            'status'=>$status,
        ]);

        if($saved){
            return redirect()->route('posts.index')->with('success','Post Successfully !');
        }
        // dd($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post=Post::findOrFail($id);

        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post=Post::findOrFail($id);

        return view('posts.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        // dd($post);

        $request->validate([
            'title' =>  'required',
            'description'    => 'required'
        ]);

        // $post=Post::findOrFail($id);

        if($request->status)
            $status=1;
        else
            $status=0;

        $post->update([
            'title' => $request->title,
            'description'    => $request->description,
            'status'    =>$status
        ]);


        if($post){
            return redirect()->route('posts.index')->with('success','Post updated successfully !');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        if($post){
            return redirect()->route('posts.index')->with('success','Pos deleted successfully !');
        }
    }

    public function SourngFunction()
    {
        return "Hello Sourng !";
    }
}
