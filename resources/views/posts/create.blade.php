@extends('master.master')
@section('content')

<div class="container">
    <div class="row mt-4">
        <div class="col-md-8 bg-success p-3">
            <form action="{{route('posts.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                {{-- {{ csrf_field() }} --}}
                <div class="form-group">
                  <label for="title">Title :</label>
                  <input type="text" class="form-control" name="title" placeholder="Enter title" id="title">
                </div>
                <div class="form-group">
                  <label for="description">Description:</label>
                  <textarea class="form-control" rows="5" id="description" name="description"></textarea>
                </div>
                <div class="form-group form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" name="status" type="checkbox"> Status
                  </label>
                </div>

                <a class="btn btn-danger" href="{{route('posts.index')}}">Back</a>
                <button type="submit" class="btn btn-primary float-right">Publish Now</button>
            </form>
        </div>
        <div class="col-md-4 bg-primary p-3">
                <h2>Upload Image</h2>
                <input type="file" class="form-control" name="feature_image" id="">
        </div>
    </div>
</div>


@endsection
