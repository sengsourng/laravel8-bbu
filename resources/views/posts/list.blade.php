@extends('master.master')
@section('content')
    <div class="container pt-4">
        <h3>List All Post       <a href="{{route('posts.create')}}" class="btn btn-success float-right"> Create</a></h3>

        @if ($message=Session::get('success'))
            <div class="alert alert-success">
                <p>{{$message}}</p>
            </div>
        @endif
        <table class="table">
        <thead>
            <tr>
            <th>ID</th>
            <th>Title</th>
            {{-- <th>Description</th> --}}
            <th>Post Date</th>
            <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts as $key => $post)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$post->title}}</td>
                    <td>
                        {{ $post->created_at->diffForHumans() }}
                    </td>
                    <td>
                        <form action="{{route('posts.destroy',$post->id)}}" method="POST">
                            <a href="{{route('posts.show',$post->id)}}" class="btn btn-primary">View</a>
                            <a href="{{route('posts.edit',$post->id)}}" class="btn btn-success">Edit</a>

                            <button type="submit" class="btn btn-danger">Delete</button>

                            @csrf
                            @method('DELETE')
                        </form>

                    </td>
                </tr>
            @endforeach

        </tbody>
        </table>

        {!! $posts->links() !!}
    </div>
@endsection
