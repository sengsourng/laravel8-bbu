@extends('master.master')

@section('content')
    <div class="container">
        <div class="list-group pt-4">
            <div class="row">
                <div class="col-md-9">
                    <h3>{{$post->title}}</h3>
                    {{$post->description}}
                    <br>
                    <a class="btn btn-success" href="{{route('posts.index')}}" class="list-group-item">
                        Back to List
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="#" class="list-group-item disabled">Category</a>
                    <a href="#" class="list-group-item disabled">Posts</a>

                </div>
            </div>

        </div>
    </div>

@endsection
