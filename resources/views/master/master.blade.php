
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{asset('bootstrap')}}/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Tutorial for BBU Master</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/jumbotron/">

    <!-- Bootstrap core CSS -->
    <link href="{{asset('bootstrap')}}/dist/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet"> --}}


    <!-- Custom styles for this template -->
    <link href="{{asset('bootstrap')}}/jumbotron.css" rel="stylesheet">
  </head>

  <body>

{{-- Navbar --}}
    @include('master.nav')

    <main role="main" class="pb-4 pt-5">

        @yield('content')

    </main>

    <footer class="container">
      <p>&copy; Company 2017-2018</p>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="{{asset('bootstrap')}}/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="{{asset('bootstrap')}}/assets/js/vendor/popper.min.js"></script>
    <script src="{{asset('bootstrap')}}/dist/js/bootstrap.min.js"></script>
  </body>
</html>
