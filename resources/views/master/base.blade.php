<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Master Blade Template</title>
    @stack('styles')
</head>
<body>
    <header>
        <h2>Master Page</h2>
    </header>
    <div style="background-color: greenyellow; width: 350px;height: 400px; float: left;padding:15px;">
        <h3>Left Navbar</h3>
    </div>
    <div style="width: 1024px; height: 400px; background-color: rgb(156, 156, 170); padding:15px;float: left;">


            {{-- <h3>Main Content</h3>
            <h4>all Content or Form here</h4> --}}
            @yield('content')

    </div>

    <footer style="width: 1024px;padding:15px;">
        <h2>Footer Page</h2>
    </footer>


    @stack('scripts')

</body>
</html>
